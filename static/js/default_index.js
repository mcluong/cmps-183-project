// We are going to try to add this file which contains the vue.js usage

var app = function(){
    var self = {}; // self from python

    Vue.config.silent = false; // Lets see how bad it looks

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    var enumerate = function(v) {
        var k=0;
        return v.map(function(e) {e._idx = k++;});
    };

    self.store_email = function (email) {
        localStorage.email = email;
    };

    self.read_email = function () {
        if (localStorage.email) {
            self.vue.email = localStorage.email;
        } else {
            self.vue.email = null;
        }
    };

    function get_posts_url(start_idx, end_idx){
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx
        };
        return posts_url + "?" + $.param(pp);
    }

    function get_userposts_url(start_idx, end_idx, email){
        var pp = {
            start_idx: start_idx,
            end_idx: end_idx,
            email: email,
        };
        return userpost_url + "?" + $.param(pp);
    }

    self.get_posts = function () {
        jQuery.getJSON(get_posts_url(0,4), function (data){
                self.vue.posts = data.posts;
                self.vue.has_more = data.has_more;
                self.vue.logged_in = data.logged_in;
                self.vue.user_email = data.user_email;
                enumerate(self.vue.posts);
            }
        )
    };

    self.get_userposts = function () {
        self.read_email();
        email = self.vue.email;
        jQuery.getJSON(get_userposts_url(0,4, email), function (data){
                self.vue.userposts = data.posts;
                self.vue.user_has_more = data.has_more;
            }
        )
    };

    self.add_post_button = function(){
        self.vue.is_adding_post = !self.vue.is_adding_post;
    };

    self.add_post = function () {
        $.post(add_post_url,
            {
                post_content: self.vue.form_post_content
            },
            function (data) {
                $.web2py.enableElement($("#add_post_submit"));
                self.vue.posts.unshift(data.post);
            });
    };

    self.add_fav_button = function(){
        self.vue.add_fav_button = !self.vue.add_fav_button;
    };

    // ADDED BY SEAN, its for followed button
    self.test_fn = function (post_title){
        for (var i = 0; i < self.vue.favorites.length; i++) {
            if (self.vue.favorites[i].post_Key === post_title || self.vue.favorites[i].postKey === post_title){
                return true;
            }
        }
        return false;
    };

    // ADDED BY SEAN, adds to favorite
    self.add_to_fav = function(post_title){
        // We need to save the post id to correspond with user
        // We need to also add to a page specifically for user
        var already_present = false;
        for (var i = 0; i < self.vue.posts.length; i++) {
            if (self.vue.posts[i].post_title == post_title) {
                already_present = true;
            }
        }

        if(already_present){
            console.log("In add_to_fav: duplicates")
        }

        $.post(add_to_fav_url,
            {
                post_key: post_title,
                user_email: self.vue.email,
            },
        function(data){
            self.vue.favorites.unshift(data.fav_list);
        }
        );
    };

    // ADDED BY SEAN, get favorites
    self.get_favorites = function(){
        $.getJSON(get_fav_url,
            function (data){
                self.vue.favorites = data.get_favs;
            }
        )
    };

    self.fav_link = function(title){
        for(var i = 0; i < self.vue.posts.length; i++) {
            if (self.vue.posts[i].post_title == title){
                return edit_url + "/" + self.vue.posts[i].id
            }
        }
    }

    self.edit_post = function(post_id) {
        return edit_url + "/" + post_id
    };

    self.delete_post = function(post_id) {
        for(var i = 0; i < self.vue.posts.length; i++) {
            if(self.vue.posts[i].id == post_id) {
                $.post(del_post_url,
                    { post_id: self.vue.posts[i].id },
                    function () {
                        self.vue.posts.splice(post_id, 1);
                        enumerate(self.vue.posts);
                    });
                self.get_posts();
            }
        }
    };

    self.voting = function(post_id, option) {
        for(var i = 0; i < self.vue.posts.length; i++) {
            if(self.vue.posts[i].id == post_id) {
                if(option == 'for') {
                    var update = self.vue.posts[i].vote_for;
                    update += 1;
                    $.post(vote_post_url,
                    {
                        For_vote: update,
                        Against_vote: self.vue.posts[i].vote_against,
                        post_id: self.vue.posts[i].id,
                    });
                }
                if(option == 'against'){
                    var update = self.vue.posts[i].vote_against;
                    update += 1;
                    $.post(vote_post_url,
                    {
                        For_vote: self.vue.posts[i].vote_for,
                        Against_vote: update,
                        post_id: self.vue.posts[i].id,
                    });
                }
            }
        }
        self.get_posts();
        self.get_userposts();
    };

    self.clear_post = function () {
        self.vue.userposts = [];
        self.vue.email = null;
    }

    self.get_more = function (){
        var num_posts = self.vue.posts.length;
        $.getJSON(get_posts_url(num_posts, num_posts + 4), function(data){
            self.vue.has_more = data.has_more;
            self.extend(self.vue.posts, data.posts);
            enumerate(self.vue.posts);
        });
    };

    self.user_get_more = function () {
        var num_posts = self.vue.userposts.length;
        self.read_email();
        email = self.vue.email;
        jQuery.getJSON(get_userposts_url(num_posts, num_posts + 4, email), function(data) {
            self.vue.user_has_more = data.has_more;
            for(var i = 0; i < data.posts.length; i++) {
                self.vue.userposts.push(data.posts[i])
            }
        });
    };

    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${','}'],
        unsafeDelimiters: ['!{','}'],
        data: {
            user_email: null,
            email: null,
            logged_in: false,
            has_more: false,
            user_has_more: false,
            is_adding_post: false,
            add_fav_button: false,
            posts : [],
            favorites: [],
            userposts: [],
        },
        methods: {
            store_email: self.store_email,
            get_more: self.get_more,
            user_get_more: self.user_get_more,
            add_post_button: self.add_post_button,
            add_fav_button: self.add_fav_button,
            edit_post: self.edit_post,
            delete_post: self.delete_post,
            voting: self.voting,
            add_to_fav: self.add_to_fav,
            get_favorites: self.get_favorites,
            test_fn: self.test_fn,
            clear_post: self.clear_post,
            fav_link: self.fav_link,
        }
    });
    self.get_posts();
    self.get_favorites();
    self.get_userposts();
    $("#vue-div").show();
    return self;
};


var APP = null;

// This will make accessible from js console
// such as self.x above would be accessible as app.x

jQuery(function(){APP=app();});

// This is a trick
// wrapping APP = app, evaluated after the document is loaded

// In javascript console, if we typed APP.vue.has_more
// we'd get the value of has_more