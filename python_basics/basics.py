"""
comment
box

"""

import os
import datetime as datetime

class TestClass(object):
    """
    """
    def __init__(self, a, b=None):
        self.a = a
        self.b = b


def sum_two_nums(a,b):
    """

    :return:
    :rtype:
    """
    return a+b

print sum_two_nums(12,535)
l2 = [i for i in xrange(10) if i%2==0]
print l2
# same as:

for i in range(10):
    if i%2==0:
        print i

print datetime.datetime.now()
print os.getcwd()
