def index():
    pass

def get_user_name_from_email(email):
    """Returns a string corresponding to the user first and last names,
    given the user email."""
    u = db(db.auth_user.email == email).select().first()
    if u is None:
        return 'None'
    else:
        return ' '.join([u.first_name, u.last_name])

def get_posts():
    # Need to get the start and stop indicies from (html?),
    # however, if can't set to zero
    # I don't know where these variables exactly come from
    start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0

    # posts will contain all the elements that will be stored into the
    # database
    posts = []
    has_more = False
    # rows are all the elements, limited by start and stop idx
    # i and row are part of enumerate, which counts +1 and gets the row respectively
    rows = db().select(
        db.Arguments_list.ALL, limitby = (start_idx, end_idx + 1), orderby=~db.Arguments_list.modified_on
    )

    for i,row in enumerate(rows):
        time = humanize_date_difference(datetime.datetime.now(),row.created_on,None)
        name = get_user_name_from_email(row.user_email)

        if i < end_idx - start_idx:
            t = dict(
                id = row.id,
                user_email = row.user_email,
                post_title = row.Argument_title,
                post_for = row.Arguments_For,
                vote_for = row.For_vote,
                post_against = row.Arguments_Against,
                vote_against = row.Against_vote,
                hash_tag=row.Argument_hashtag,
                created_on = time,
                first_name = name,
            )
            posts.append(t)
        else:
            has_more = True

    logged_in = auth.user_id is not None
    if logged_in:
    	user = auth.user.email
    else:
    	user = ""
    # I'm not sure where these are being used,
    # in the java_script_test, the logged_in variable was mispelt, so it might
    # not even be used anywhere.
    return response.json(
        dict(
            posts = posts,
            logged_in = logged_in,
            has_more = has_more,
            user_email = user,
        )
    )

# Note that we need the URL to be signed, as this changes the db.
#@auth.requires_signature()
#def add_post():
#    t_id = db.Arguments_list.insert(
#        Argument_title = request.vars.post_content
#        Arguments_For =
#        Arguments_Against =
#    )
#    t = db.Arguments_list(t_id)
#    return response.json(dict(post=t))

@auth.requires_signature()
def del_post():
    db(db.Arguments_list.id == request.vars.post_id).delete()
    return "ok"

def get_userpost():
    start_idx = int(request.vars.start_idx) if not None else 0
    end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
    email = request.vars.email
    posts = []
    has_more = False
    rows = db(db.Arguments_list.user_email == email).select(
        db.Arguments_list.ALL, limitby=(start_idx, end_idx + 1), orderby=~db.Arguments_list.created_on
    )
    for i, row in enumerate(rows):
        if i < end_idx - start_idx:
            t = dict(
                id=row.id,
                user_email=row.user_email,
                post_title=row.Argument_title,
                post_for=row.Arguments_For,
                vote_for=row.For_vote,
                post_against=row.Arguments_Against,
                vote_against=row.Against_vote,
                hash_tag=row.Argument_hashtag,
                created_on=row.created_on,
            )
            posts.append(t)
        else:
            has_more = True
    return response.json(dict(
        posts=posts,
        has_more = has_more,
    ))

@auth.requires_signature()
def add_userpost():
    t_id = db.users_post.insert(
        users_json=request.vars.order_json
    )
    t = db.users_post(t_id)
    return response.json(dict(userpost=t))

@auth.requires_signature()
def vote_post():
    t = db(db.Arguments_list.id == request.vars.post_id).update(
        For_vote=request.vars.For_vote,
        Against_vote=request.vars.Against_vote,
    )
    return response.json(dict(post=t))

# Added by Sean to add a favorite quick access bar
def add_to_fav():
    t_id = db.users_favorites.insert(
        postKey = request.vars.post_key
    )
    t = db.users_favorites(t_id)
    return response.json(dict(fav_list=t))

def get_fav():
    favArray = []
    rows = db().select(db.users_favorites.ALL)
    for row in rows:
        t = dict(
            id = row.id,
            user_email = row.user_email,
            post_Key = row.postKey,
        )
        favArray.append(t)
    return response.json(dict(
        get_favs = favArray,
    ))

def humanize_date_difference(now, otherdate=None, offset=None):
    """
    From https://gist.github.com/nzjrs/207624
    Humanizes time between two datetimes

    :param now:
    :param otherdate:
    :param offset:
    :return:

    :param now:
    :param otherdate:
    :param offset:
    :return:
    """
    if otherdate:
        dt = now - otherdate
        offset = dt.seconds + (dt.days * 60*60*24)
    if offset is not None:
        delta_s = offset % 60
        offset /= 60
        delta_m = offset % 60
        offset /= 60
        delta_h = offset % 24
        offset /= 24
        delta_d = offset
    else:
        raise ValueError("Must supply otherdate or offset (from now)")

    if delta_d > 1:
        if delta_d > 6:
            date = now + datetime.timedelta(days=-delta_d, hours=-delta_h, minutes=-delta_m)
            return date.strftime('%A, %Y %B %m, %H:%I')
        else:
            wday = now + datetime.timedelta(days=-delta_d)
            return wday.strftime('%A')
    if delta_d == 1:
        return "Yesterday"
    if delta_h > 0:
        return "%d hours ago" % (delta_h)
    if delta_m > 0:
        return "%d minutes ago" % (delta_m)
    else:
        return "%d seconds ago" % delta_s

def pretty_date(time=False):
    """
    Get a datetime object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """
    from datetime import datetime
    now = datetime.utcnow()
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return "a minute ago"
        if second_diff < 3600:
            return str(second_diff / 60) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str(second_diff / 3600) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff / 7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff / 30) + " months ago"
    return str(day_diff / 365) + " years ago"

