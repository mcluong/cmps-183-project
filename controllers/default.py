# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# -------------------------------------------------------------------------
# This is a sample controller
# - index is the default action of any application
# - user is required for authentication and authorization
# - download is for downloading files uploaded in the db (does streaming)
# -------------------------------------------------------------------------

# This is defined in a module. See modules/random_text.py
import random_text
import json

def add_to_pages(s):
    if session.visited_pages is None:
        session.visited_pages = [s]
    elif s not in session.visited_pages:
        session.visited_pages.append(s)

def has_been_at(s):
    return session.visited_pages is not None and s in session.visited_pages

def index():
    #Tracking we have been here
    add_to_pages('index')
    argument_list = None
    if auth.user_id is not None:
        argument_list = db().select(
            orderby= ~db.Arguments_list.modified_on
        )

    return dict(title='Decision Maker',
                place = 'Initial page',
                arguments_list = argument_list,
                )

def index1():
    db.Arguments_list.created_on.default = request.now
    return locals()

def profile():
    add_to_pages('profile')
    argument_list = None
    if auth.user_id is not None:
        argument_list = db(db.Arguments_list.user_email == auth.user.email).select(
            orderby=~db.Arguments_list.modified_on
        )

    return dict(title='Decision Maker',
                place='Initial page',
                arguments_list=argument_list,
                )

def show():
    form = db.Arguments_list(request.args(0, cast=int))
    db.args_comments1.Arguments_list.default = form.id
    db.args_comments1.Arguments_list.readable = False
    db.args_comments1.Arguments_list.writable = False
    newForm = SQLFORM(db.args_comments1).process()
    comments = db(db.args_comments1.Arguments_list==form.id).select()
    return locals()

@auth.requires_login()
def create_argument():
    db.Arguments_list.created_on.default = request.now
    form = SQLFORM(db.Arguments_list)
    if form.process().accepted:
        session.flash = T('Argument Added') #store info, flashed on screen, convenient method for action
        redirect(URL('default','index'))
    elif form.errors:
        session.flash = T('Enter Correct Values Please')
    return dict(form=form)

@auth.requires_login()
def edit():
    if request.args(0) is None:
        form_type = 'create'
        form = SQLFORM(db.Arguments_list)
        redirect('create_argument')
    else:
        q = (db.Arguments_list.user_email == auth.user.email) &\
            (db.Arguments_list.id == request.args(0))
        cl = db(q).select().first()
        if cl is None:
            session.flash = T('Not Authorized')
            redirect(URL('default','index'))

        cl.modified_on = datetime.datetime.utcnow()
        cl.update_record()

        is_edit = (request.vars.edit == 'true')
        form_type = 'edit' if is_edit else 'view'
        form = SQLFORM(db.Arguments_list, record=cl, deletable=is_edit, readonly=not is_edit)
        # Theres a code snippet at the bottom to be inserted later, JSON
    button_list = []
    if form_type == 'edit':
        button_list.append(A('Cancel', _class='btn btn-warning',
                             _href=URL('default', 'edit', args=[cl.id])))
    elif form_type == 'create':
        button_list.append(A('Cancel', _class='btn btn-warning',
                             _href=URL('default','index')))

    if form.process().accepted:
        #At this point the record has already been inserted.
        if form_type == 'create':
            session.flash = T('Argument added.')
        else:
            session.flash = T('Argument edited.')
        redirect(URL('default','index'))
    elif form.errors:
        session.flash = T('Please enter correct values.')

    return dict(form=form, button_list=button_list, cl=cl)



@auth.requires_login()
def index2():
    if not ( has_been_at('index') and has_been_at('index1') ):
        return('go away')
    return dict(place="Page 2")

@auth.requires_membership('managers')
def manage():
    grid = SQLFORM.grid(db.Arguments_list)
    return locals()

@auth.requires_login()
def toggle_sold():
     item = db.forsale(request.vars(0)) or redirect(URL('default', 'index'))
     item.update_record(sold = not item.sold)
     redirect(URL('default', 'index')) # Assuming this is where you want to go

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()



'''
try:
    item_list = json.loads(cl.Argument_For)
except:
    pass
if not isinstance(item_list,list):
    if isinstance(cl.Argument_For, basestring):
        item_list = [cl.Argument_For]
    else:
        item_list = []

form = SQLFORM.factory(
    Field('title', default = cl.Argument_title),
    Field('Items', 'list:string', default = item_list)
)
'''