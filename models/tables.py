# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.

import datetime

db.define_table('Arguments_list',
                Field('user_email', default=auth.user.email if auth.user_id else None),
                Field('Argument_title', 'string'),
                Field('Arguments_For', 'list:string'),
                Field('For_vote', 'integer', default = 0),
                Field('Arguments_Against', 'list:string'),
                Field('Against_vote', 'integer', default = 0),
                Field('Argument_hashtag', 'string'),
                auth.signature,
                )

# Don't display user email all the time
db.Arguments_list.user_email.readable = db.Arguments_list.user_email.writable = False
# Make sure that at least the title is not empty
db.Arguments_list.Argument_title.requires = IS_NOT_EMPTY()
# Make sure the created on is not changeable
db.Arguments_list.created_on.writable = False
db.Arguments_list.For_vote.readable = db.Arguments_list.For_vote.writable = False
db.Arguments_list.Against_vote.readable = db.Arguments_list.Against_vote.writable = False

# after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

db.define_table('args_comments1',
                Field('Arguments_list','reference Arguments_list'),
                Field('comment_content', 'text', requires=IS_NOT_EMPTY() ),
                auth.signature,
                )

#order history
db.define_table('users_post',
    Field('user_email', default=auth.user.email if auth.user_id else None),
    Field('created_on', 'datetime', default=datetime.datetime.utcnow()),
    Field('users_json', 'text'),  # Order information, in json
)

db.define_table('users_favorites',
                Field('user_email', default=auth.user.email if auth.user_id else None),
                Field('postKey', 'string')
                )